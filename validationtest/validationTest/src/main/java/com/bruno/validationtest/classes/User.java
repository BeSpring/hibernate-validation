package com.bruno.validationtest.classes;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
@UniqueUsername(message="Password must have a value!")
public class User {

	private Integer id;

	@Size(min = 3, message = "Name must be at least 3 characters!")
	private String name;

	@Size(min = 1, message = "Invalid email address!")
	@Email(message = "Invalid email address!")
	private String email;
	@Size(min = 5, message = "Password must be at least 5 characters!")

	private String password;

	private boolean enabled;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}

package com.bruno.validationtest.classes;


import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/validation")
public class ValidatorController {




	@PostMapping("/test1")
	public String validiamo1(@RequestBody @Validated Person person) {

		return "ciao è passato";

	}
	
	@PostMapping("/test2")
	public String validiamo2(@RequestBody @Valid User user) {

		return "ciao è passato";

	}
}

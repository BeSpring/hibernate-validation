package com.bruno.validationtest.classes;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, User> {

	@Override
	public boolean isValid(User user, ConstraintValidatorContext context) {
		if (user.getName() == null && user.getPassword() == null) {
			return false;
		}
		return true;
	}

}

package com.bruno.validationtest.classes;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class PersonValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Person.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
		Person p = (Person) obj;

		if (p.getAge() == null && p.getName() == null) {
			e.rejectValue("age","name", "cannot be null");
			e.rejectValue("name", "cannot be null");

		}
	}

}

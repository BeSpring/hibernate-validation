package com.bruno.validationtest.classes;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class Person {
	@NotNull
	@NotEmpty
	@Size(min = 2, max = 14)
	
	private String name;
	@NotNull
	@NotEmpty
	private String age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

}

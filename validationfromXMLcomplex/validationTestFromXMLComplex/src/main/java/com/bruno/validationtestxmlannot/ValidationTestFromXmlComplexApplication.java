package com.bruno.validationtestxmlannot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidationTestFromXmlComplexApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidationTestFromXmlComplexApplication.class, args);
	}

}

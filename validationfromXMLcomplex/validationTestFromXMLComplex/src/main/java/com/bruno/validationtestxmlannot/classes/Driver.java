package com.bruno.validationtestxmlannot.classes;



public class Driver extends Person {
	public int age;
	public boolean hasDrivingLicense;

	public Driver(String name) {
		super(name);
	}

	public void passedDrivingTest(boolean b) {
		hasDrivingLicense = b;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}

package com.bruno.validationtestxmlannot.classes;

import javax.validation.GroupSequence;

@GroupSequence({ RentalChecks.class, CarChecks.class, RentalCar.class })
public class RentalCar extends Car {
	private boolean rented;

	public RentalCar(String manufacturer, String licencePlate, int seatCount) {
		super(manufacturer, licencePlate, seatCount);
	}

	public boolean isRented() {
		return rented;
	}

	public void setRented(boolean rented) {
		this.rented = rented;
	}
}

package com.bruno.validationtestxmlannot.classes;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { ValidPassengerCountValidator.class })
@Documented
public @interface ValidPassengerCount {
	String message() default "{com.bruno.validationtestxmlannot.classes." + "ValidPassengerCount.message}";
	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
